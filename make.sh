mkdir -p build
mkdir -p test/build
gcc -c ./src/slurper.c -l curl -o ./build/slurper.o
gcc -c ./src/yyjson/src/yyjson.c -o ./build/yyjson.o
gcc ./test/src/test_slurper.c -l curl -L './build' -l:yyjson.o -l:slurper.o -o ./test/build/test_slurper
